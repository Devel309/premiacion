<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campania extends Model
{
    protected $table = "campania";
    protected $fillable = ["idcampania", "descripcion"];
}
