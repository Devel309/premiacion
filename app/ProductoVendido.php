<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoVendido extends Model
{
    protected $table = "premiosempleado";
    protected $fillable = ["idempleado", "idcampania", "descripcionPremio", "lugarPrestacion", "fechaRegistro", "fechaVencimiento"];
}
