<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipificacion extends Model
{
    protected $table = "tipificacion";
    protected $fillable = ["idpremio", "tipificacion", "fechaRegistro", "observacion"];
}
