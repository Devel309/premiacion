<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Premiosempleado extends Model
{
    protected $table = "premiosempleado";
    protected $fillable = ["idempleado", "idcampania", "descripcionPremio", "lugarPrestacion", "fechaRecojo", "fechaVencimiento"];
}
