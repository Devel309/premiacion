<?php

namespace App\Http\Controllers;

use App\Tipificacion;
use Illuminate\Http\Request;

class TipificacionController extends Controller
{

    public function store(Request $request)
    {
        $tipificar = new Tipificacion($request->input());
        $tipificar->saveOrFail();
        return redirect()->route("productos.index")->with("mensaje", "Tipificado");
    }
}
