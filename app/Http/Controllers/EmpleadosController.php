<?php

namespace App\Http\Controllers;
use DB;
use App\Premiosempleado;
use App\Empleado;
use App\Campania;
use Illuminate\Http\Request;
use Carbon\Carbon;

class EmpleadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $premios = DB::table('premiosempleado')
            ->join('empleado', 'premiosempleado.idempleado', '=', 'empleado.idempleado')
            ->join('campania', 'premiosempleado.idcampania', '=', 'campania.idcampania')
            ->select('premiosempleado.*', 'empleado.nombre', 'campania.descripcion', 'tipificacion.tipificacion', 'premiosempleado.fechaRecojo')
            ->leftJoin("tipificacion", "premiosempleado.idpremio", "=", "tipificacion.idpremio")
            ->orderBy('tipificacion.fechaRegistro', 'desc')
            ->get();
        return view("empleados.empleados_index", ["premios" => $premios, "empleados" => Empleado::all()]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("empleados.empleados_create", ["empleados" => Empleado::all(), "campanias" => Campania::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $premio = new Premiosempleado($request->input());
        $premio->saveOrFail();
        return redirect()->route("empleados.index")->with("mensaje", "Premio empleado guardado");
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Premiosempleado $premiosempleado
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $premios = DB::table('premiosempleado')
            ->join('empleado', 'premiosempleado.idempleado', '=', 'empleado.idempleado')
            ->join('campania', 'premiosempleado.idcampania', '=', 'campania.idcampania')
            ->select('premiosempleado.*', 'empleado.nombre', 'campania.descripcion', 'tipificacion.tipificacion', 'premiosempleado.fechaRecojo')
            ->leftJoin("tipificacion", "premiosempleado.idpremio", "=", "tipificacion.idpremio")
            ->where('premiosempleado.idempleado', $request->empleado)
            ->whereBetween('premiosempleado.fechaRecojo', [$request->fechaRegistroUno, $request->fechaRegistroDos])
            ->orderBy('tipificacion.fechaRegistro', 'desc')
            ->get();
        return view("empleados.empleados_index", ["premios" => $premios, "empleados" => Empleado::all()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Premiosempleado $premiosempleado
     * @return \Illuminate\Http\Response
     */
    public function edit(string $idpremio)
    {
        $premio = DB::table('premiosempleado')
            ->join('empleado', 'premiosempleado.idempleado', '=', 'empleado.idempleado')
            ->join('campania', 'premiosempleado.idcampania', '=', 'campania.idcampania')
            ->select('premiosempleado.*', 'empleado.nombre', 'campania.descripcion')
            ->where('idpremio', $idpremio)
            ->first();
        return view("empleados.empleados_edit", ["premio" => $premio]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Premiosempleado $premiosempleado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Premiosempleado $premiosempleado
     * @return \Illuminate\Http\Response
     */
    public function destroy(Premiosempleado $producto)
    {
    }
}
