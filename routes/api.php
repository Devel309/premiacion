<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Premiosempleado;
use App\Empleado;
use App\Venta;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get("/status", function () {
    return Auth::guard('api')->check();
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

        Route::get("productos", function () {
            return response()->json(Premiosempleado::all());
        });
        /*
            Si existe un dios, que me perdone por dejar todas las peticiones aquí
            en lugar de separarlas a otro archivo o invocar un controlador
        */
        Route::post("/producto", function(Request $request){
            $producto = new Premiosempleado($request->input());
            $producto->saveOrFail();
            return response()->json(["data" => "true"]);
        });
        Route::get("/producto/{id}", function($id){
            $producto = Premiosempleado::findOrFail($id);
            return response()->json($producto);
        });
        Route::put("/producto", function(Request $request){
            $producto = Premiosempleado::findOrFail($request->input("id"));
            $producto->fill($request->input());
            $producto->saveOrFail();
            return response()->json(true);
        });
        Route::delete("/producto/{id}", function($id){
            $producto = Premiosempleado::findOrFail($id);
            $producto->delete();
            return response()->json(true);
        });

        // Empleado
        
        Route::get("clientes", function () {
            return response()->json(Empleado::all());
        });
        Route::post("/cliente", function(Request $request){
            $cliente = new Empleado($request->input());
            $cliente->saveOrFail();
            return response()->json(["data" => "true"]);
        });
        Route::get("/cliente/{id}", function($id){
            $cliente = Empleado::findOrFail($id);
            return response()->json($cliente);
        });

        // Ventas
        Route::get("ventas", function () {
            return response()->json(Venta::with(["productos", "cliente"])->get());
        });
        Route::post("/venta", function(Request $request){
            $venta = new Venta($request->input());
            $venta->saveOrFail();
            return response()->json(["data" => "true"]);
        });
        Route::get("/venta/{id}", function($id){
            $venta = Venta::with(["productos", "cliente"])->findOrFail($id);
            return response()->json($venta);
        });
        Route::delete("/venta/{id}", function($id){
            $venta = Venta::findOrFail($id);
            $venta->delete();
            return response()->json(true);
        });

    });
});
