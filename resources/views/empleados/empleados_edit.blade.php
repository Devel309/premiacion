@extends("maestra")
@section("titulo", "Formulario de Tipificaciones - premio empleado")
@section("contenido")
    <div class="row">
        <div class="col-12">
            <h1>Formulario de Tipificaciones</h1>
                <div class="form-group">
                    <label class="label">Empleado</label>
                    <input required value="{{$premio->nombre}}" autocomplete="off" name="codigo_barras"
                           class="form-control" readonly
                           type="text" placeholder="Código de barras">
                </div>
                <div class="form-group">
                    <label class="label">Campania</label>
                    <input required value="{{$premio->descripcion}}" autocomplete="off" name="codigo_barras"
                           class="form-control" readonly
                           type="text" placeholder="Código de barras">
                </div>
                <div class="form-group">
                    <label class="label">Descripción Premio</label>
                    <input required value="{{$premio->descripcionPremio}}" autocomplete="off" name="descripcion"
                           class="form-control" readonly
                           type="text" placeholder="Descripción">
                </div>
                <div class="form-group">
                    <label class="label">Lugar de Recojo</label>
                    <input required value="{{$premio->lugarPrestacion}}" autocomplete="off" name="precio_compra"
                           class="form-control" readonly
                           type="text" placeholder="Precio de compra">
                </div>
                <div class="form-group">
                    <label class="label">Fecha de Recojo</label>
                    <input required value="{{$premio->fechaRecojo}}" autocomplete="off" name="precio_venta"
                           class="form-control" readonly
                           type="date" placeholder="Precio de venta">
                </div>
                <div class="form-group">
                    <label class="label">Fecha de Vencimiento</label>
                    <input required value="{{$premio->fechaVencimiento}}" autocomplete="off" name="existencia"
                           class="form-control" readonly
                           type="date" placeholder="Existencia">
                </div>
            <form method="POST" action="{{route("tipificacion.store")}}">
                @csrf
                <div class="form-group">
                    <input hidden autocomplete="off" name="idpremio" class="form-control"
                           type="text" value="{{ $premio->idpremio}}">
                </div>

                <div class="form-group">
                    <label class="label">Tipificacion</label>
                    <select required class="form-control" name="tipificacion" id="tipificacion">
                        <option value="1">Informado correctamente</option>
                        <option value="2">No informado</option>
                        <option value="3">Nuevo </option>
                    </select>
                </div>

                <div class="form-group">
                    <input hidden autocomplete="off" name="fechaRegistro" class="form-control"
                           type="text" value="{{ date('Y-m-d') }}">
                </div>

                <div class="form-group">
                    <label class="label">Observacion</label>
                    <input required autocomplete="off" name="observacion" class="form-control"
                           class="form-control"
                           type="text" placeholder="observacion">
                </div>

                @include("notificacion")
                <button class="btn btn-success">Guardar</button>
                <a class="btn btn-primary" href="{{route("empleados.index")}}">Volver</a>
            </form>
        </div>
    </div>
@endsection
