@extends("maestra")
@section("titulo", "Premios Empleados")
@section("contenido")
    <div class="row">
        <div class="col-12">
            <h1>Premios Empleados <i class="fa fa-box"></i></h1>
            <a href="{{route("empleados.create")}}" class="btn btn-success mb-2">Agregar</a>
            <form method="GET" action="{{route("empleados.show", 'show')}}">
                @csrf
                <div class="form-group col-md-12">
                    <div class="col-md-3">
                        <label class="label">Nombre Empleado</label>
                        <select required class="form-control" name="empleado" id="empleado">
                            @foreach($empleados as $emp)
                                <option value="{{$emp->idempleado}}">{{$emp->nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label class="label">Fecha de Registro</label>
                        <input autocomplete="off" name="fechaRegistroUno" class="form-control"
                            type="date">
                    </div>
                    <div class="col-md-3">
                        <input autocomplete="off" name="fechaRegistroDos" class="form-control"
                            type="date">
                    </div>
                </div>
                <button class="btn btn-danger">Buscar</button>
            </form>
            @include("notificacion")
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Empleado</th>
                        <th>Descripción del premio</th>
                        <th>Campania</th>
                        <th>Fecha de recojo</th>
                        <th>Lugar de recojo</th>
                        <th>Fecha de vencimiento</th>

                        <th>Tipificacion</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($premios as $premio)
                        <tr>
                            <td>{{$premio->idpremio}}</td>
                            <td>{{$premio->nombre}}</td>
                            <td>{{$premio->descripcionPremio}}</td>
                            <td>{{$premio->descripcion}}</td>
                            <td>{{$premio->fechaRecojo}}</td>
                            <td>{{$premio->lugarPrestacion}}</td>
                            <td>{{$premio->fechaVencimiento}}</td>
                            <td>
                                <a class="btn btn-warning" href="{{route("empleados.edit",[$premio->idpremio])}}">
                                    Seguimiento
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
