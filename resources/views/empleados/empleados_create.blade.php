@extends("maestra")
@section("titulo", "Agregar premios empleados")
@section("contenido")
    <div class="row">
        <div class="col-12">
            <h1>Agregar premios empleados</h1>
            <form method="POST" action="{{route("empleados.store")}}">
                @csrf
                <div class="form-group">
                    <label class="label">Empleado</label>
                    <select required class="form-control" name="idempleado" id="idEmpleado">
                        @foreach($empleados as $emp)
                            <option value="{{$emp->idempleado}}">{{$emp->nombre}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="label">Campania</label>
                    <select required class="form-control" name="idcampania" id="idCampania">
                        @foreach($campanias as $camp)
                            <option value="{{$camp->idcampania}}">{{$camp->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="label">Descripción Premio</label>
                    <input required autocomplete="off" name="descripcionPremio" class="form-control"
                           type="text" placeholder="Descripción">
                </div>
                <div class="form-group">
                    <label class="label">Lugar de Recojo</label>
                    <input required autocomplete="off" name="lugarPrestacion" class="form-control"
                           type="text" placeholder="Precio de compra">
                </div>
                <div class="form-group">
                    <label class="label">Fecha de Recojo</label>
                    <input required autocomplete="off" name="fechaRecojo" class="form-control"
                           type="date" placeholder="Precio de venta">
                </div>
                <div class="form-group">
                    <label class="label">Fecha de Vencimiento</label>
                    <input required autocomplete="off" name="fechaVencimiento" class="form-control"
                           type="date" placeholder="Precio de venta">
                </div>

                @include("notificacion")
                <button class="btn btn-success">Guardar</button>
                <a class="btn btn-primary" href="{{route("empleados.index")}}">Volver al listado</a>
            </form>
        </div>
    </div>
@endsection
