<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TipificacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipificacion', function (Blueprint $table) {
            $table->integer("idpremio")
                ->references("idpremio")
                ->on("premioEmpleado")
                ->onDelete("cascade")
                ->onUpdate("cascade");
            $table->integer("tipificacion");
            $table->date("fechaRegistro")->useCurrent();
            $table->string("observacion");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
