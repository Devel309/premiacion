<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PremiosEmpleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premiosEmpleado', function (Blueprint $table) {
            $table->integer("idpremio")->primary();
            $table->integer("idempleado")
                ->references("idempleado")
                ->on("empleado")
                ->onDelete("cascade")
                ->onUpdate("cascade");
            $table->integer("idcampania")
                ->references("idcampania")
                ->on("campania")
                ->onDelete("cascade")
                ->onUpdate("cascade");
            $table->string("descripcionPremio");
            $table->string("lugarPrestacion");
            $table->date("fechaRecojo")->useCurrent();
            $table->date("fechaVencimiento");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
